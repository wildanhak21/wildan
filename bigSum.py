from flask import Flask, request
app = Flask(__name__)

@app.route('/verybigsum', methods=['POST'])
def veryBigSum():
    ar = request.get_json['array']
    sum = 0
    i = 0

    while i < len(ar):
        sum += ar[i]
        i += 1

    return {'Hasilnya' : sum}

