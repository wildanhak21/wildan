from flask import Flask, request
app = Flask(__name__)

@app.route('/plusminus', methods=['POST'])
def plusMinus():
    arr = request.get_json()['data']
    positif = 0
    negatif = 0
    zero = 0

    for i in range(len(arr)):
        if arr[i] > 0:
            positif += 1
        elif arr[i] < 0:
            negatif += 1
        else:
            zero += 1

    hasil_p = positif / len(arr)
    hasil_n = negatif / len(arr)
    hasil_zero = zero / len(arr)
    return{'Jumlah Positif' : hasil_p, 'Jumlah Negatif' : hasil_n, 'Jumlah kosong' : hasil_zero}