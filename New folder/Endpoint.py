from flask import Flask, request
from Function import *
import mergesort

app = Flask(__name__)

@app.route('/binary_search/<int:target>', methods=['GET','POST'])
binarySearch(target)

@app.route('/timeconversion/', methods=['GET','POST'])
timeConversion()

@app.route('/drawingbook/', methods=['GET','POST'])
drawingBook()

@app.route('/numberline/', methods=['GET','POST'])
numberLine()

@app.route('/mergesort/', methods=['GET','POST'])
mergesort.merge()