def binarySearch(target):
    list = request.get_json()['data']
    i = 0
    x = target
    list.sort()
    while i < len(list):
        i += 1
        if list[int(len(list) / 2)] == x:
            return {'Data Ditemukan ': x, 'Pencarian ke': i}
        if list[int(len(list) / 2)] < x:
            list = list[int(len(list) / 2):]
        elif list[int(len(list) / 2)] > x:
            list = list[:int(len(list) / 2)]
        if len(list) == 1 and list[0] != x:
            return {'Data tidak ditemukan' : x}


def timeConversion():
    time = request.get_json()['waktu']
    x = time.split(':')
    if time[-2:] == 'PM':
            if x[0] != '12':
                x[0] = str(int(x[0])+12)
    elif time[-2:] == 'AM':
            if x[0] == '12':
                x[0] = '00'
    return {'Waktunya' : ':'.join(x)}


def drawingBook():
  req = request.get_json()
  jmlhlm = req['halaman']
  tujuanhlm = req['target']

  x = jmlhlm//2
  s = tujuanhlm//2
  hasil = min(s,x-s)
  return {'Target bisa dicapai dalam' : str(hasil)}


def numberLine():
    req = request.get_json()
    x1 = req['kang1']
    v1 = req['jump1']
    x2 = req['kang2']
    v2 = req['jump2']
    max = 10000
    hasil = False
    i = 0
    while i<=max:
        i += 1
        x1 = x1 + v1
        x2 = x2 + v2
        if (x1 == x2):
            hasil = True

    if hasil:
        return {'hasilnya' : 'YES'}
    else:
        return {'hasilnya' : 'NO'}