from flask import Flask
from flask import request

app = Flask(__name__)

@app.route('/')
def hello():
	return 'Hello World'

@app.route('/timeconversion/<input>')
def timeConversion(input):
  	x = input.split(':')
  	if input[-2:] == 'PM':
    		if x[0] != '12':
        		x[0] = str(int(x[0])+12)
  	elif input[-2:] == 'AM':
    		if x[0] == '12':
        		x[0] = '00'
  	return 'Waktunya :' + ':'.join(x)

@app.route('/drawingbook/<int:jmlhlm>/<int:tujuanhlm>')
def drawingBook(jmlhlm,tujuanhlm):
  x = jmlhlm//2
  s = tujuanhlm//2 
  hasil = min(s,x-s)
  return str(hasil)