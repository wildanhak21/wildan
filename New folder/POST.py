from flask import Flask, request
app = Flask(__name__)

@app.route('/')
def hello():
    print('Hello')

@app.route('/binary_search/<int:target>', methods=['GET','POST'])
def binarySearch(target):
    list = request.get_json()['data']
    i = 0
    x = target
    list.sort()
    while i < len(list):
        i += 1
        if list[int(len(list) / 2)] == x:
            return {'Data Ditemukan ': x, 'Pencarian ke': i}
        if list[int(len(list) / 2)] < x:
            list = list[int(len(list) / 2):]
        elif list[int(len(list) / 2)] > x:
            list = list[:int(len(list) / 2)]
        if len(list) == 1 and list[0] != x:
            return {'Data tidak ditemukan' : x}

