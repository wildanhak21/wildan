from flask import Flask, request
app = Flask(__name__)

@app.route('/timeconversion/', methods=['GET','POST'])
def timeConversion():
    time = request.get_json()['waktu']
    x = time.split(':')
    if time[-2:] == 'PM':
            if x[0] != '12':
                x[0] = str(int(x[0])+12)
    elif time[-2:] == 'AM':
            if x[0] == '12':
                x[0] = '00'
    return {'Waktunya' : ':'.join(x)}

@app.route('/drawingbook/', methods=['GET','POST'])
def drawingBook():
  req = request.get_json()
  jmlhlm = req['halaman']
  tujuanhlm = req['target']

  x = jmlhlm//2
  s = tujuanhlm//2
  hasil = min(s,x-s)
  return {'Target bisa dicapai dalam' : str(hasil)}

@app.route('/numberline/', methods=['GET','POST'])
def kangoroo():
    req = request.get_json()
    x1 = req['kang1']
    v1 = req['jump1']
    x2 = req['kang2']
    v2 = req['jump2']
    max = 10000
    hasil = False
    i = 0
    while i<=max:
        i += 1
        x1 = x1 + v1
        x2 = x2 + v2
        if (x1 == x2):
            hasil = True

    if hasil:
        return {'hasilnya' : 'YES'}
    else:
        return {'hasilnya' : 'NO'}