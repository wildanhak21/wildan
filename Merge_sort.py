from flask import Flask, request
app = Flask(__name__)

@app.route('/mergesort/', methods=['GET','POST'])
def get_data():
    data = request.get_json()['array']
    x = merge_sort(data)
    return {'Hasil Sort' : x}

def merge_sort(data):
    if len(data) <= 1:
        return data
    mid = int(len(data) // 2)
    left = merge_sort(data[mid:])
    right = merge_sort(data[:mid])
    return merge_sort2(left,right)

def merge_sort2(left,right):
    hasil = []
    l_index = 0
    r_index = 0
    while l_index < len(left) and r_index < len(right):
       if left[l_index] < right[r_index]:
          hasil.append(left[l_index])
          l_index += 1
       else:
          hasil.append(right[r_index])
          r_index += 1

    hasil.extend(left[l_index:])
    hasil.extend(right[r_index:])
    return hasil

if __name__ == '__main__':
    app.run(debug=True)