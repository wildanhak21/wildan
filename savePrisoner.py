from flask import Flask, request
app = Flask(__name__)

@app.route('/saveprisoner', methods=['POST'])
def saveThePrisoner():
    req = request.get_json()
    n = req['jml_prisoner']
    m = req['jml_candy']
    s = req['f_point']

    bad = s + m - 1
    if (bad > n):
        if (bad % n == 0):
            return {'Prisoner ke' : n}
        else:
            return {'Prisoner ke' : bad % n}
    else:
        return {'Prisoner ke' : bad}
