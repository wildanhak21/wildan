from flask import Flask, request
app = Flask(__name__)

@app.route('/comparetriplets', methods=['POST'])
def compareTriplets():
    req = request.get_json()
    a = req['data1']
    b = req['data2']
    skor_a = 0
    skor_b = 0

    for i in range(len(a)):
        if a[i] < b[i]:
            skor_b += 1
        elif a[i] > b[i]:
            skor_a += 1

    hasil = [skor_a, skor_b]
    return {'Hasilnya' : hasil}